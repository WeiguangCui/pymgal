Installation
==================

This guide will walk you through the necessary prerequisites, installation steps, and how to run the code for PyMGal.


Installing stable version
-------------
We are working on registering PyMGal with the Python Package Index (PyPI). Once this is done, PyMGal will be installable with pip. Until then, please install the developer version.

Installing developer version
-------------
To install the latest version, you can clone the repository with git. 

.. code-block:: python

   git clone https://bitbucket.org/pjanul/pymgal
   pip install /your/path/to/pymgal
   
  
Prerequisites
-------------

To install the necessary dependencies, simply enter your/path/to/pymgal (i.e. the outer PyMGal directory) and run the following at the command line.

.. code-block:: python

   pip install requirements.txt
   
 